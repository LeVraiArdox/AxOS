# Changelog
_________
**24.02** (yeah i changed the version format and started writing in english)
- Rework on the global style
- Installation now works (at 99%)
- Removed useless apps from the ISO
- Removed AxShell
- Removed blur effects in terminal and file manager
- Now using AxOS hooks (a lot of cool stuff)
- Better compatibility with AMD and NVIDIA drivers

_____________________
**2.7.0 - 05/06/2023 11:26**
- Correction totale des bugs d'installation du système
- Amelioration de l'ergonomie globale
- correction de nombreux bugs de performance
- prise en charge (facultative) du chaotic aur
- amelioration dans le style global
- ajout de nombreux logiciels, dont la suite libre Office et AudioTube (alternative a spotify sans pub)
- Modification du lanceur d'applications
- Modification du menu global de l'application
- Beacoup d'ameliorations dans le skel

______________________________
**2.6.0 - 15/04/2023 22:35**

- Correction de bugs dû au copytoram
- Amelioration des performances 
- Purification de l'image ISO et du système
- correction de bugs sur AxShell
- Application des modifications AxOS pour Calamares Framework
_ _ _ _ _ _ _ _ _ _ _ _
**2.5.9 - 29/03/2023 11:40**

- Ajout du thème grub et Plymouth de AxOS
- Correction du bug 127 de l'installateur ALCI
- Correction de bugs graphiques (écran qui clignote)
- Amélioration du skel de AxOS
- Compatibilité de drivers pour les pc unowhy Y13 et Y14
- prise en charge de BetterDiscord 

___________________________
**2.8.0 - 08/03/2023**
- la correction de bugs 
- le passage du shell bash par défaut au Zshell (zsh)
- Correction de certaines erreures de alci
___________________________

***No changelog for older versions***
